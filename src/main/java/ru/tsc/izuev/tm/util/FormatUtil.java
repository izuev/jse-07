package ru.tsc.izuev.tm.util;

import java.text.DecimalFormat;

public interface FormatUtil {

    static String formatBytes(long bytes) {
        final long kilobyte = 1024;
        final long megabyte = kilobyte * 1024;
        final long gigabyte = megabyte * 1024;
        final long terabyte = gigabyte * 1024;
        DecimalFormat dF = new DecimalFormat("#.###");

        if ((bytes >= 0) && (bytes < kilobyte)) {
            return bytes + " B";
        } else if ((bytes >= kilobyte) && (bytes < megabyte)) {
            return dF.format((float) bytes / kilobyte) + " KB";
        } else if ((bytes >= megabyte) && (bytes < gigabyte)) {
            return dF.format((float) bytes / megabyte) + " MB";
        } else if ((bytes >= gigabyte) && (bytes < terabyte)) {
            return dF.format((float) bytes / gigabyte) + " GB";
        } else if (bytes >= terabyte) {
            return dF.format((float) bytes / terabyte) + " TB";
        } else {
            return bytes + " Bytes";
        }
    }

}
