package ru.tsc.izuev.tm;

import ru.tsc.izuev.tm.constant.ArgumentConst;
import ru.tsc.izuev.tm.constant.CommandConst;
import ru.tsc.izuev.tm.util.TerminalUtil;

import static ru.tsc.izuev.tm.util.FormatUtil.formatBytes;

public class Application {

    public static void main(String[] args) {
        processArguments(args);
        processCommands();
    }

    private static void processCommands() {
        System.out.println("*** WELCOME TO TACK MANAGER ***");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND: ");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private static void exit() {
        System.exit(0);
    }

    private static void showErrorArgument() {
        System.out.println("[ERROR]");
        System.err.println("Current program arguments are not correct...");
        System.exit(1);
    }

    private static void showErrorCommand() {
        System.out.println("[ERROR]");
        System.err.println("Current command is not correct...");
    }

    private static void processArguments(final String[] arguments) {
        if (arguments == null || arguments.length < 1) return;
        processArgument(arguments[0]);
        exit();
    }

    private static void processCommand(final String argument) {
        switch (argument) {
            case CommandConst.VERSION:
                showVersion();
                break;
            case CommandConst.ABOUT:
                showAbout();
                break;
            case CommandConst.INFO:
                showSystemInfo();
                break;
            case CommandConst.HELP:
                showHelp();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                showErrorCommand();
        }
    }

    private static void processArgument(final String argument) {
        switch (argument) {
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.INFO:
                showSystemInfo();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            default:
                showErrorArgument();
        }
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.7.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Igor Zuev");
        System.out.println("E-mail: izuev@t1-consulting.ru");
    }

    private static void showSystemInfo() {
        System.out.println("[SYSTEM INFO]");
        final int processorCount = Runtime.getRuntime().availableProcessors();
        System.out.println("PROCESSORS: " + processorCount);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long usedMemory = totalMemory - freeMemory;

        System.out.println("MAX MEMORY: " + formatBytes(maxMemory));
        System.out.println("TOTAL MEMORY: " + formatBytes(totalMemory));
        System.out.println("FREE MEMORY: " + formatBytes(freeMemory));
        System.out.println("USED MEMORY: " + formatBytes(usedMemory));
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s - Show version info.\n", CommandConst.VERSION, ArgumentConst.VERSION);
        System.out.printf("%s, %s - Show developer info.\n", CommandConst.ABOUT, ArgumentConst.ABOUT);
        System.out.printf("%s, %s - Show system info.\n", CommandConst.INFO, ArgumentConst.INFO);
        System.out.printf("%s, %s - Show command list.\n", CommandConst.HELP, ArgumentConst.HELP);
        System.out.printf("%s - Close application.\n", CommandConst.EXIT);
    }

}